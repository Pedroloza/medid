package facci.pedro.lozano.mediapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Button;

import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

public class registrouwu extends AppCompatActivity {
    private EditText MeditName;
    private EditText Meditpellido ;
    private EditText lefono;
    private EditText  email;

EditText emailID,password;
Button btnSingup;
TextView tvSigmin;
FirebaseAuth mFirebaseAuth;
Button registrar;



private String nombreuf = " ";
    private String apellido = " ";
    private String telefono = " ";

    private String passwd = " ";


    DatabaseReference mDatabase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrouwu);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mFirebaseAuth = FirebaseAuth.getInstance();
        emailID = findViewById(R.id.email);
        password = findViewById(R.id.pasword);
        btnSingup= findViewById(R.id.subirinformacion);
        tvSigmin = findViewById(R.id.vistadelcreate);
        btnSingup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String email = emailID.getText().toString();
                String pwd = password.getText().toString();
                if (email.isEmpty()){
                emailID.setError("Porfavor coloque su Email");
                emailID.requestFocus();

                }
                 else if (pwd.isEmpty()){
                    password.setError("Porfavor coloque su contraseña");
                    password.requestFocus();
                }
                 else if(email.isEmpty() && pwd.isEmpty()){
                        Toast.makeText(registrouwu.this,"Fields are empty!",Toast.LENGTH_SHORT).show();

                }
                 else if (!(email.isEmpty() && pwd.isEmpty())){
                     mFirebaseAuth.createUserWithEmailAndPassword(email,pwd).addOnCompleteListener(registrouwu.this, new OnCompleteListener<AuthResult>() {
                         @Override
                         public void onComplete(@NonNull Task<AuthResult> task) {
                             if (!task.isSuccessful()){

                                 Map<String,Object> map = new HashMap<>();
                                 map.put("editName",nombreuf);
                                 map.put("email",email);
                                 map.put("telefono",telefono);
                                 map.put("Apellido",apellido);

                                 map.put("Password",password);


                                 String id =mFirebaseAuth.getCurrentUser().getUid();
                                 mDatabase.child("Usuarios").child(id).setValue(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                                     @Override
                                     public void onComplete(@NonNull Task<Void> task) {
                                         if(task.isSuccessful()){
                                             startActivity(new Intent(registrouwu.this,Inicio.class));

                                         }
                                     }
                                 });
                                 Toast.makeText(registrouwu.this,"registro sin exito,Intente otra vez",Toast.LENGTH_SHORT).show();
                             }
                             else {
                                 startActivity(new Intent(registrouwu.this,Inicio.class));
                             }
                         }
                     });

                     }
                 else {
                    Toast.makeText(registrouwu.this,"Error Ocurrido!",Toast.LENGTH_LONG).show();
                }
            }
        });

        tvSigmin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i  =new Intent(registrouwu.this, loginuwu.class);
                    startActivity(i);
                }



        });


        }
    }

