package facci.pedro.lozano.mediapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.google.firebase.auth.FirebaseAuth;

public class Inicio extends AppCompatActivity {

    ImageView medicina;
    ImageView ajustes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);

        medicina =findViewById(R.id.loginmedicina);
        ajustes =findViewById(R.id.loginajustes);

        medicina.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 =new Intent  (Inicio.this, medicina.class);
                startActivity(intent1);
            }
        });
        ajustes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent2 =new Intent  (Inicio.this, ajustes.class);
                startActivity(intent2);
            }
        });
    }
}
