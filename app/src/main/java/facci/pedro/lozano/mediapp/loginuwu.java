package facci.pedro.lozano.mediapp;

import androidx.annotation.NonNull;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class loginuwu extends AppCompatActivity {
    EditText emailID,password;
    Button btnSingin;
    TextView tvSigmup;
    FirebaseAuth mFirebaseAuth;
    private  FirebaseAuth.AuthStateListener mAuthStateListener;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loginuwu);

        mFirebaseAuth = FirebaseAuth.getInstance();
        emailID = findViewById(R.id.text1);
        password = findViewById(R.id.text2);
        btnSingin= findViewById(R.id.buttonuwu);
        tvSigmup = findViewById(R.id.laweadeabajo);

            mAuthStateListener = new FirebaseAuth.AuthStateListener() {
                @Override
                public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                    FirebaseUser mFirebaseUser = mFirebaseAuth.getCurrentUser();
            if (mFirebaseUser!=null){
                Toast.makeText(loginuwu.this, "Ya estas logeado uwu", Toast.LENGTH_SHORT).show();
                Intent i  =new Intent(loginuwu.this,Inicio.class);
                startActivity(i);
            }

            else{

                Toast.makeText(loginuwu.this, "Please logeate", Toast.LENGTH_SHORT).show();

            }
                }
            };
            btnSingin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String email = emailID.getText().toString();
                    String pwd = password.getText().toString();
                    if (email.isEmpty()){
                        emailID.setError("Porfavor coloque su Email");
                        emailID.requestFocus();

                    }
                    else if (pwd.isEmpty()){
                        password.setError("Porfavor coloque su contraseña");
                        password.requestFocus();

                    }
                    else if (email.isEmpty()&& pwd.isEmpty()) {
                        Toast.makeText(loginuwu.this,"Fields are empty!",Toast.LENGTH_LONG).show();

                    }
                    else if (!(email.isEmpty()&& pwd.isEmpty())){
                        mFirebaseAuth.signInWithEmailAndPassword(email,pwd).addOnCompleteListener(loginuwu.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (!task.isSuccessful()){
                                    Toast.makeText(loginuwu.this,"Error Ocurrido , Porfavor Intente Nuevamente",Toast.LENGTH_LONG).show();


                                }
                                else {
                                    Intent intToHome = new Intent(loginuwu.this,Inicio.class);
                                    startActivity(intToHome);
                                }
                            }
                        });
                    }
                    else {
                        Toast.makeText(loginuwu.this,"Error ocurrido!",Toast.LENGTH_SHORT);
                    }
                }
            });


        tvSigmup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent IntSignUp = new Intent (loginuwu.this, registrouwu.class  );
                startActivity(IntSignUp);
            }
        });
    }
    protected void onStart (){
        super.onStart();
        mFirebaseAuth.addAuthStateListener(mAuthStateListener);
    }
}
