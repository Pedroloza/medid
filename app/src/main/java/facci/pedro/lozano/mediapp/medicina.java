package facci.pedro.lozano.mediapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class medicina extends AppCompatActivity {
    ImageView atras2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medicina);

        atras2 =(ImageView)findViewById(R.id.buttonatras2);

        atras2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 =new Intent(medicina.this, Inicio.class);
                startActivity(intent1);
            }
        });
    }
}
